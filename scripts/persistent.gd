extends Node2D

var levels = []
var current_level = 0
const NB_LEVELS = 5

func _ready():
	reinit()

func reinit():
	levels.clear()
	current_level = 0
	for i in range(0, NB_LEVELS):
		levels.push_back( {"coins_collected" : null, "total_coins" : null, "nb_death" : null, "done" : false} )

func save():
	var levels_str = {} # level : 0, coins_collected : 0, total_coins : 0
	var count = 0 # 0 is tutorial
	for lvl in levels :
		levels_str[str(count)] = lvl
		count += 1
	
	# Open file
	var save = File.new()
	save.open( "user://save_game.save", File.WRITE )
	save.store_line( levels_str.to_json() )
	save.close()
	print("Game saved.")
	
func load_a_save():
	var save = File.new()
	save.open("user://save_game.save", File.READ)
	var lines = {}
	lines.parse_json( save.get_line() )
	print("lines = " , lines)
	levels.clear()
#	for line in lines :
	for i in range(0, NB_LEVELS) :
		var line = lines[str(i)]
		print("loading line : " , line)
		var coins_col  = 0
		if line["coins_collected"] : coins_col = int(line["coins_collected"])
		
		var tot_coins  = 0
		if line["total_coins"] : tot_coins = int(line["total_coins"])
		
		var death = 0
		if line["nb_death"] : death = int(line["nb_death"])
		
		var done = bool(line["done"])
		
		levels.push_back( {
		"coins_collected" : coins_col
		, "total_coins" : tot_coins
		, "nb_death" : death
		, "done" : done
		} ) 
	pass
	