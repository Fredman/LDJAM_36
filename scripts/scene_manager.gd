extends Node2D

var current_scene = null
onready var root = get_tree().get_root()


func _ready():
	current_scene = root.get_child( root.get_child_count() - 1 )

func goto_instance( i ):
	call_deferred("_goto_instance", i)
	
func _goto_instance( i ):
	get_tree().get_current_scene().free()
	print("Scene Manager has clean current scene.")
	current_scene = i
	get_tree().get_root().add_child(i)
	get_tree().set_current_scene(i)
	
	